package com.inditex.challenge.prices.application.controller.exception;

import com.inditex.challenge.prices.domain.core.exception.BrandNotFoundException;
import com.inditex.challenge.prices.domain.core.exception.PriceNotFoundException;
import com.inditex.challenge.prices.domain.core.exception.ProductNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.resource.NoResourceFoundException;
import shaded_package.javax.validation.ConstraintViolation;
import shaded_package.javax.validation.ConstraintViolationException;
import shaded_package.javax.validation.ValidationException;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorDto handleException(Exception exception) {
        log.error(exception.getMessage(), exception);
        return new ErrorDto(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(), "Unexpected error!");
    }

    @ResponseBody
    @ExceptionHandler(value = {ValidationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(ValidationException validationException) {
        ErrorDto error;
        if (validationException instanceof ConstraintViolationException) {
            String violations = extractViolationsFromException((ConstraintViolationException) validationException);
            log.error(violations, validationException);
            error = new ErrorDto(HttpStatus.BAD_REQUEST.getReasonPhrase(), violations);
        } else {
            String exceptionMessage = validationException.getMessage();
            log.error(exceptionMessage, validationException);
            error = new ErrorDto(HttpStatus.BAD_REQUEST.getReasonPhrase(), exceptionMessage);
        }
        return error;
    }

    @ResponseBody
    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(MissingServletRequestParameterException missingServletRequestParameterException) {
        String message = missingServletRequestParameterException.getMessage();
        return new ErrorDto(HttpStatus.BAD_REQUEST.getReasonPhrase(), message);
    }

    @ResponseBody
    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto handleException(MethodArgumentTypeMismatchException methodArgumentTypeMismatchException) {
        String message = methodArgumentTypeMismatchException.getMessage();
        return new ErrorDto(HttpStatus.BAD_REQUEST.getReasonPhrase(), message);
    }

    private String extractViolationsFromException(ConstraintViolationException validationException) {
        return validationException.getConstraintViolations()
                .stream()
                .map(ConstraintViolation::getMessage)
                .collect(Collectors.joining("--"));
    }

    @ResponseBody
    @ExceptionHandler(value = {NoResourceFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleException(NoResourceFoundException noResourceFoundException) {
        log.error(noResourceFoundException.getMessage(), noResourceFoundException);
        return new ErrorDto(HttpStatus.NOT_FOUND.getReasonPhrase(), noResourceFoundException.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {BrandNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleException(BrandNotFoundException brandNotFoundException) {
        log.error(brandNotFoundException.getMessage(), brandNotFoundException);
        return new ErrorDto(HttpStatus.NOT_FOUND.getReasonPhrase(), brandNotFoundException.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {PriceNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleException(PriceNotFoundException priceNotFoundException) {
        log.error(priceNotFoundException.getMessage(), priceNotFoundException);
        return new ErrorDto(HttpStatus.NOT_FOUND.getReasonPhrase(), priceNotFoundException.getMessage());
    }

    @ResponseBody
    @ExceptionHandler(value = {ProductNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto handleException(ProductNotFoundException productNotFoundException) {
        log.error(productNotFoundException.getMessage(), productNotFoundException);
        return new ErrorDto(HttpStatus.NOT_FOUND.getReasonPhrase(), productNotFoundException.getMessage());
    }
}
