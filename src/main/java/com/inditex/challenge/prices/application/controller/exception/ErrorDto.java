package com.inditex.challenge.prices.application.controller.exception;


public record ErrorDto(String code, String message) {
}
