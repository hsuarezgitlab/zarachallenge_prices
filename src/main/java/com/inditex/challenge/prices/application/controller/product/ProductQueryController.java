package com.inditex.challenge.prices.application.controller.product;

import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import com.inditex.challenge.prices.domain.ports.service.QueryApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.ZonedDateTime;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductQueryController {
    private final QueryApplicationService queryService;

    @GetMapping("{product_id}/price")
    public FindPriceQueryResponse findPrice(
            @PathVariable(name = "product_id") int productId,
            @RequestParam(name = "applied_datetime") ZonedDateTime appliedDateTime,
            @RequestParam(name = "brand_id") int brandId
    ) {
        FindPriceQueryRequest findPriceQuery = FindPriceQueryRequest.builder()
                .productId(productId)
                .brandId(brandId)
                .appliedDateTime(appliedDateTime)
                .build();
        return queryService.findPriceByProductBrandAppliedDate(findPriceQuery);
    }
}
