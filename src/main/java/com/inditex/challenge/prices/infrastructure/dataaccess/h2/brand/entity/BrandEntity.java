package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity;

import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "BRAND")
public class BrandEntity implements Serializable{
    @Serial
    private static final long serialVersionUID = -6909437083649341589L;
    @Id
    private int id;
    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "brand", cascade = CascadeType.ALL)
    private List<PriceEntity> prices;
}
