package com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity;

import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

@Entity
@Data
@Table(name = "PRICE")
public class PriceEntity implements Serializable {
    @Serial
    private static final long serialVersionUID = -3820731623997259645L;
    @Id
    private int id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "BRAND_ID")
    private BrandEntity brand;
    private ZonedDateTime startDate;
    private ZonedDateTime endDate;
    private int priceList;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "PRODUCT_ID")
    private ProductEntity product;
    private int priority;
    private BigDecimal price;
    private String curr;

}
