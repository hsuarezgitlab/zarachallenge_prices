package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity;

import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import jakarta.persistence.*;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Entity
@Data
@Table(name = "PRODUCT")
public class ProductEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = -3186488904255463366L;

    @Id
    private int id;

    private String name;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "product", cascade = CascadeType.ALL)
    private List<PriceEntity> prices;
}
