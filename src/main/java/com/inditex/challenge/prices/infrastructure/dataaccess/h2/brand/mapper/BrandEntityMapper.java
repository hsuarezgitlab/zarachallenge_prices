package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.mapper;

import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandName;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import org.springframework.stereotype.Component;

@Component
public class BrandEntityMapper {
    public Brand brandEntityToBrand(BrandEntity brandEntity) {
        return Brand.Builder.newBuilder()
                .id(new BrandId(brandEntity.getId()))
                .name(new BrandName(brandEntity.getName()))
                .build();
    }

}
