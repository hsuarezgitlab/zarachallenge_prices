package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.adapter;


import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.ports.repository.BrandRepository;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.mapper.BrandEntityMapper;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.repository.BrandJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class BrandRepositoryImpl implements BrandRepository {
    private final BrandJpaRepository brandJpaRepository;
    private final BrandEntityMapper brandEntityMapper;
    @Override
    public Optional<Brand> findById(BrandId brandId) {
        Optional<BrandEntity> brandEntity = brandJpaRepository.findById(brandId.getValue());
        return brandEntity.map(brandEntityMapper::brandEntityToBrand);
    }
}
