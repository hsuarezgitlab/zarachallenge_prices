package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.mapper;

import com.inditex.challenge.prices.domain.core.entity.Price;
import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.price.*;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductName;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ProductEntityMapper {

    public ProductAggregate productEntityToProductAggregate(ProductEntity productEntity) {
        return ProductAggregate.Builder.newBuilder()
                .id(new ProductId(productEntity.getId()))
                .name(new ProductName(productEntity.getName()))
                .prices(priceEntitiesToPrices(productEntity.getPrices()))
                .build();
    }

    public List<Price> priceEntitiesToPrices(List<PriceEntity> prices) {
        return prices.stream().map(this::priceEntityToPrice).toList();
    }

    public Price priceEntityToPrice(PriceEntity priceEntity) {
        return Price.Builder.newBuilder()
                .id(new PriceId(priceEntity.getId()))
                .brandId(new BrandId(priceEntity.getBrand().getId()))
                .startDate(new StartDate(priceEntity.getStartDate()))
                .endDate(new EndDate(priceEntity.getEndDate()))
                .priceList(new PriceList(priceEntity.getPriceList()))
                .priority(new Priority(priceEntity.getPriority()))
                .price(priceEntity.getPrice())
                .currency(new Currency(priceEntity.getCurr()))
                .build();
    }
}
