package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.repository;

import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductJpaRepository extends JpaRepository<ProductEntity, Integer> {

}
