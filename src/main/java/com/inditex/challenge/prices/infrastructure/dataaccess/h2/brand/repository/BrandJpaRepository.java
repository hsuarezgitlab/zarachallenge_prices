package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.repository;

import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.ZonedDateTime;
import java.util.Optional;

public interface BrandJpaRepository extends JpaRepository<BrandEntity, Integer> {
}
