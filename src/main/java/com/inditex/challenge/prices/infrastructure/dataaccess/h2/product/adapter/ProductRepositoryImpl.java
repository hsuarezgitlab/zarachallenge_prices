package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.adapter;

import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.ports.repository.ProductRepository;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.mapper.ProductEntityMapper;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.repository.ProductJpaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepository {
    private final ProductJpaRepository productJpaRepository;
    private final ProductEntityMapper entityMapper;

    @Override
    public Optional<ProductAggregate> findById(ProductId productId) {
        Optional<ProductEntity> productEntity = productJpaRepository.findById(productId.getValue());
        return productEntity.map(entityMapper::productEntityToProductAggregate);
    }
}
