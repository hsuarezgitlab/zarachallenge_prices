package com.inditex.challenge.prices.domain.ports.service;

import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import jakarta.validation.Valid;

public interface QueryApplicationService {
    FindPriceQueryResponse findPriceByProductBrandAppliedDate(@Valid FindPriceQueryRequest findPriceQuery);
}
