package com.inditex.challenge.prices.domain.core.exception;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;

public class BrandNotFoundException extends DomainException{
    public BrandNotFoundException(BrandId brandId) {
        super(String.format("brand with id: %s doesn't exist", brandId.getValue()));
    }
}
