package com.inditex.challenge.prices.domain.ports.repository;

import java.util.Optional;

public interface Repository <T, ID>{
    Optional<T> findById(ID id);
}
