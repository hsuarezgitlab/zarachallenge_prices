package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class Currency extends BaseObjectValue<String> {
    public Currency(String value) {
        super(value);
    }
}
