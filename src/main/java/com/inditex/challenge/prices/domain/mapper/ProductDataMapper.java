package com.inditex.challenge.prices.domain.mapper;

import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;

@Component
public class ProductDataMapper {
    public FindPriceQueryResponse productToFindPriceQueryResponse(ProductAggregate product, ZonedDateTime appliedDate, BrandId brandId) {
        return new FindPriceQueryResponse(
                product.getId().getValue(),
                product.getBrandIdAtSpecificDate(appliedDate, brandId),
                product.getPriceListAtSpecificDate(appliedDate, brandId),
                product.getPriceStartDateAtSpecificDate(appliedDate, brandId),
                product.getPriceEndDateAtSpecificDate(appliedDate, brandId),
                product.getPriceAtSpecificDate(appliedDate, brandId)
        );
    }
}
