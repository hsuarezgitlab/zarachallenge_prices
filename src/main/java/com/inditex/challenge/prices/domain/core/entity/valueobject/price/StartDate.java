package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

import java.time.ZonedDateTime;

public class StartDate extends BaseObjectValue<ZonedDateTime> {
    public StartDate(ZonedDateTime value) {
        super(value);
    }
}