package com.inditex.challenge.prices.domain;

import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.core.exception.BrandNotFoundException;
import com.inditex.challenge.prices.domain.core.exception.ProductNotFoundException;
import com.inditex.challenge.prices.domain.ports.repository.BrandRepository;
import com.inditex.challenge.prices.domain.ports.repository.ProductRepository;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class PriceQueryHelperImpl implements PriceQueryHelper{
    private final ProductRepository productRepository;
    private final BrandRepository brandRepository;
    @Override
    public ProductAggregate findPriceByProductBrandAppliedDate(FindPriceQueryRequest findPriceQuery) {

        BrandId brandId = new BrandId(findPriceQuery.getBrandId());
        brandRepository.findById(brandId).orElseThrow(() -> new BrandNotFoundException(brandId));

        ProductId productId = new ProductId(findPriceQuery.getProductId());
        return productRepository.findById(productId).orElseThrow(()-> new ProductNotFoundException(productId));
    }
}
