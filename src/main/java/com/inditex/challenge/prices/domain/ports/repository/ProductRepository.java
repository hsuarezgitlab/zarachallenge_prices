package com.inditex.challenge.prices.domain.ports.repository;

import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;

import java.util.Optional;

public interface ProductRepository extends Repository<ProductAggregate, ProductId> {
    Optional<ProductAggregate> findById(ProductId productId);
}
