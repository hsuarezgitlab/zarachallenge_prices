package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

import java.time.ZonedDateTime;

public class EndDate extends BaseObjectValue<ZonedDateTime> {
    public EndDate(ZonedDateTime value) {
        super(value);
    }
}
