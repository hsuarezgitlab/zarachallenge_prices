package com.inditex.challenge.prices.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import shaded_package.javax.validation.constraints.NotNull;

import java.time.ZonedDateTime;

@Builder
@Data
public class FindPriceQueryRequest {
    @NotNull
    @JsonProperty(namespace = "product_id")
    private final int productId;
    @NotNull
    @JsonProperty(namespace = "brand_id")
    private final int brandId;
    @NotNull
    @JsonProperty(namespace = "applied_datetime")
    private final ZonedDateTime appliedDateTime;
}
