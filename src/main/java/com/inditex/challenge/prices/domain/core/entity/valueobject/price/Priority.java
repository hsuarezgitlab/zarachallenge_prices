package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class Priority extends BaseObjectValue<Integer> {
    public Priority(Integer value) {
        super(value);
    }
}
