package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class PriceList extends BaseObjectValue<Integer> {
    public PriceList(Integer value) {
        super(value);
    }
}
