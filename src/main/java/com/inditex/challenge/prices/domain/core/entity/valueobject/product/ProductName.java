package com.inditex.challenge.prices.domain.core.entity.valueobject.product;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class ProductName extends BaseObjectValue<String> {
    public ProductName(String value) {
        super(value);
    }
}
