package com.inditex.challenge.prices.domain.core.exception;

public class DomainException extends RuntimeException{
    public DomainException(String message) {
        super(message);
    }
}
