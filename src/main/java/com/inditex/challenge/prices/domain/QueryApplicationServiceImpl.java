package com.inditex.challenge.prices.domain;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import com.inditex.challenge.prices.domain.mapper.ProductDataMapper;
import com.inditex.challenge.prices.domain.ports.service.QueryApplicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Validated
@RequiredArgsConstructor
public class QueryApplicationServiceImpl implements QueryApplicationService {
    private final PriceQueryHelper priceQueryHelper;
    private final ProductDataMapper productDataMapper;
    @Override
    public FindPriceQueryResponse findPriceByProductBrandAppliedDate(FindPriceQueryRequest findPriceQuery) {
        return productDataMapper.productToFindPriceQueryResponse(
                priceQueryHelper.findPriceByProductBrandAppliedDate(findPriceQuery),
                findPriceQuery.getAppliedDateTime(),
                new BrandId(findPriceQuery.getBrandId())
        );
    }
}
