package com.inditex.challenge.prices.domain.core.entity.valueobject.brand;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class BrandId extends BaseObjectValue<Integer> {
    public BrandId(Integer value) {
        super(value);
    }
}
