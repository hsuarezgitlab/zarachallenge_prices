package com.inditex.challenge.prices.domain.core.entity.valueobject.brand;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class BrandName extends BaseObjectValue<String> {
    public BrandName(String value) {
        super(value);
    }
}
