package com.inditex.challenge.prices.domain.core.entity;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandName;

public class Brand extends BaseEntity<BrandId> {
    private final BrandName name;

    private Brand(Builder builder) {
        super.setId(builder.id);
        name = builder.name;
    }


    public static final class Builder {
        private BrandId id;
        private BrandName name;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(BrandId val) {
            id = val;
            return this;
        }

        public Builder name(BrandName val) {
            name = val;
            return this;
        }

        public Brand build() {
            return new Brand(this);
        }
    }
}
