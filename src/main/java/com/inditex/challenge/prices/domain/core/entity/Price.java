package com.inditex.challenge.prices.domain.core.entity;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.price.*;

import java.math.BigDecimal;

public class Price extends  BaseEntity<PriceId> {
    private final BrandId brandId;
    private final StartDate startDate;
    private final EndDate endDate;
    private final PriceList priceList;
    private final Priority priority;
    private final BigDecimal price;
    private final Currency currency;

    private Price(Builder builder) {
        super.setId(builder.id);
        brandId = builder.brandId;
        startDate = builder.startDate;
        endDate = builder.endDate;
        priceList = builder.priceList;
        priority = builder.priority;
        price = builder.price;
        currency = builder.currency;
    }


    public static final class Builder {
        private PriceId id;
        private BrandId brandId;
        private StartDate startDate;
        private EndDate endDate;
        private PriceList priceList;
        private Priority priority;
        private BigDecimal price;
        private Currency currency;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(PriceId val) {
            id = val;
            return this;
        }

        public Builder brandId(BrandId val) {
            brandId = val;
            return this;
        }

        public Builder startDate(StartDate val) {
            startDate = val;
            return this;
        }

        public Builder endDate(EndDate val) {
            endDate = val;
            return this;
        }

        public Builder priceList(PriceList val) {
            priceList = val;
            return this;
        }

        public Builder priority(Priority val) {
            priority = val;
            return this;
        }

        public Builder price(BigDecimal val) {
            price = val;
            return this;
        }

        public Builder currency(Currency val) {
            currency = val;
            return this;
        }

        public Price build() {
            return new Price(this);
        }
    }

    public BrandId getBrandId() {
        return brandId;
    }

    public StartDate getStartDate() {
        return startDate;
    }

    public EndDate getEndDate() {
        return endDate;
    }

    public PriceList getPriceList() {
        return priceList;
    }

    public Priority getPriority() {
        return priority;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Currency getCurrency() {
        return currency;
    }
}
