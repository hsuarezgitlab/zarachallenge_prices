package com.inditex.challenge.prices.domain.core.exception;

import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;

public class ProductNotFoundException extends DomainException{
    public ProductNotFoundException(ProductId productId) {
        super(String.format("product with id: %s doesn't exist", productId.getValue()));
    }
}
