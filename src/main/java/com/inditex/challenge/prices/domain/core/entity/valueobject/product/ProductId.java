package com.inditex.challenge.prices.domain.core.entity.valueobject.product;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class ProductId extends BaseObjectValue<Integer> {
    public ProductId(Integer value) {
        super(value);
    }
}