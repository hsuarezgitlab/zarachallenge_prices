package com.inditex.challenge.prices.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public record FindPriceQueryResponse(
        @JsonProperty("product_id") int productId,
        @JsonProperty("brand_id")  int brandId,
        @JsonProperty("price_list") int priceList,
        @JsonProperty("start_date") ZonedDateTime startDate,
        @JsonProperty("end_date") ZonedDateTime endDate,
        @JsonProperty("price") BigDecimal price) {
}
