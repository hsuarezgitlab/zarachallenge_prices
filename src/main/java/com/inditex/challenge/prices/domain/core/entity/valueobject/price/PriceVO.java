package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

import java.math.BigDecimal;

public class PriceVO extends BaseObjectValue<BigDecimal> {
    public PriceVO(BigDecimal value) {
        super(value);
    }
}
