package com.inditex.challenge.prices.domain.core.entity;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductName;
import com.inditex.challenge.prices.domain.core.exception.PriceNotFoundException;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.List;

public class ProductAggregate extends AggregateRoot<ProductId>{
    private final ProductName name;
    private final List<Price> prices;

    private ProductAggregate(Builder builder) {
        super.setId(builder.id);
        name = builder.name;
        prices = builder.prices;
    }

    private Price getPriceRecordAppliedToDateAndBrand(ZonedDateTime appliedDate, BrandId brandId) {
        return prices
                .stream()
                .filter(price -> price.getBrandId().equals(brandId))
                .filter(price -> price.getStartDate().getValue().isBefore(appliedDate))
                .filter(price -> price.getEndDate().getValue().isAfter(appliedDate))
                .max(Comparator.comparing(Price::getPriority, Comparator.comparing(BaseObjectValue::getValue)))
                .orElseThrow(()-> new PriceNotFoundException(this.getId(), brandId, appliedDate));
    }

    public BigDecimal getPriceAtSpecificDate(ZonedDateTime appliedDate, BrandId brandId) {
        return this.getPriceRecordAppliedToDateAndBrand(appliedDate, brandId).getPrice();
    }

    public int getBrandIdAtSpecificDate(ZonedDateTime appliedDate, BrandId brandId) {
        return this.getPriceRecordAppliedToDateAndBrand(appliedDate, brandId).getBrandId().getValue();
    }

    public int getPriceListAtSpecificDate(ZonedDateTime appliedDate, BrandId brandId) {
        return this.getPriceRecordAppliedToDateAndBrand(appliedDate, brandId).getPriceList().getValue();
    }

    public ZonedDateTime getPriceStartDateAtSpecificDate(ZonedDateTime appliedDate, BrandId brandId) {
        return this.getPriceRecordAppliedToDateAndBrand(appliedDate, brandId).getStartDate().getValue();
    }

    public ZonedDateTime getPriceEndDateAtSpecificDate(ZonedDateTime appliedDate, BrandId brandId) {
        return this.getPriceRecordAppliedToDateAndBrand(appliedDate, brandId).getEndDate().getValue();
    }

    public static final class Builder {
        private ProductId id;
        private ProductName name;
        private List<Price> prices;

        private Builder() {
        }

        public static Builder newBuilder() {
            return new Builder();
        }

        public Builder id(ProductId val) {
            id = val;
            return this;
        }

        public Builder name(ProductName val) {
            name = val;
            return this;
        }

        public Builder prices(List<Price> val) {
            prices = val;
            return this;
        }

        public ProductAggregate build() {
            return new ProductAggregate(this);
        }
    }
}
