package com.inditex.challenge.prices.domain.core.exception;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;

import java.time.ZonedDateTime;

public class PriceNotFoundException extends DomainException{
    public PriceNotFoundException(ProductId productId, BrandId brandId, ZonedDateTime appliedDate) {
        super(String.format("price not found by product: %s and brand: %s at datetime: %s" , productId.getValue(), brandId.getValue(), appliedDate));
    }
}
