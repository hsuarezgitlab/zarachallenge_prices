package com.inditex.challenge.prices.domain.core.entity.valueobject.price;

import com.inditex.challenge.prices.domain.core.entity.valueobject.BaseObjectValue;

public class PriceId extends BaseObjectValue<Integer> {
    public PriceId(Integer value) {
        super(value);
    }
}
