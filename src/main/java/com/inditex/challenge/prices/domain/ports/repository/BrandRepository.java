package com.inditex.challenge.prices.domain.ports.repository;

import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;

import java.util.Optional;

public interface BrandRepository extends Repository<Brand, BrandId> {
    Optional<Brand> findById(BrandId brandId);
}
