package com.inditex.challenge.prices.domain;

import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;

public interface PriceQueryHelper {
    ProductAggregate findPriceByProductBrandAppliedDate(FindPriceQueryRequest findPriceQuery);
}
