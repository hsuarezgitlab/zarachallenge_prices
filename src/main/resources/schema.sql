CREATE TABLE IF NOT EXISTS BRAND
(
    ID   int          not null AUTO_INCREMENT,
    NAME varchar(100) not null,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS PRODUCT
(
    ID   int          not null AUTO_INCREMENT,
    NAME varchar(100) not null,
    PRIMARY KEY (ID)
);

CREATE TABLE IF NOT EXISTS PRICE
(
    ID       int not null AUTO_INCREMENT,
    BRAND_ID int not null,
    START_DATE timestamp not null,
    END_DATE timestamp not null,
    PRICE_LIST int not null,
    PRODUCT_ID int not null,
    PRIORITY int not null,
    PRICE numeric (5,2) not null,
    CURR varchar(3) not null,
    PRIMARY KEY (ID),
    CONSTRAINT fk_BRAND_ID_PRICE
        FOREIGN KEY (BRAND_ID)
            REFERENCES BRAND (ID),
    CONSTRAINT fk_PRODUCT_ID_PRICE
        FOREIGN KEY (PRODUCT_ID)
            REFERENCES PRODUCT (ID)
);