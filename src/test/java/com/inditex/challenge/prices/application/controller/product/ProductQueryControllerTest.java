package com.inditex.challenge.prices.application.controller.product;

import com.inditex.challenge.prices.domain.core.exception.ProductNotFoundException;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import com.inditex.challenge.prices.domain.ports.service.QueryApplicationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ProductQueryController.class)
class ProductQueryControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private QueryApplicationService service;

    @Test
    void givenFindPriceQueryRequest_whenPriceExists_thenReturnFindPriceQueryResponse() throws Exception {
        // Given
        FindPriceQueryRequest findPriceQueryRequest = FindPriceQueryRequest.builder()
                .productId(1)
                .brandId(1)
                .appliedDateTime(ZonedDateTime.parse("2020-06-16T21:00:00.000Z"))
                .build();

        FindPriceQueryResponse expectedFindPriceQueryResponse =
                new FindPriceQueryResponse(
                        1,
                        1,
                        4,
                        ZonedDateTime.parse("2020-06-15T13:00:00-03:00"),
                        ZonedDateTime.parse("2020-06-15T13:00:00-03:00"),
                        BigDecimal.valueOf(38.95));

        when(service.findPriceByProductBrandAppliedDate(findPriceQueryRequest)).thenReturn(expectedFindPriceQueryResponse);

        // When
        mvc.perform(MockMvcRequestBuilders
                        .get("/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_dattime}",
                                1,1,"2020-06-16T21:00:00.000Z")
                        .contentType(MediaType.APPLICATION_JSON))

                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.product_id").value(1))
                .andExpect(jsonPath("$.brand_id").value(1))
                .andExpect(jsonPath("$.price_list").value(4))
                .andExpect(jsonPath("$.start_date").value("2020-06-15T13:00:00-03:00"))
                .andExpect(jsonPath("$.end_date").value("2020-06-15T13:00:00-03:00"))
                .andExpect(jsonPath("$.price").value(38.95));
        verify(service, times(1)).findPriceByProductBrandAppliedDate(findPriceQueryRequest);
    }

    @Test
    void givenFindPriceQueryRequest_whenProductNotExists_thenReturnNotFound() throws Exception {
        // Given
        FindPriceQueryRequest findPriceQueryRequest = FindPriceQueryRequest.builder()
                .productId(1)
                .brandId(1)
                .appliedDateTime(ZonedDateTime.parse("2020-06-16T21:00:00.000Z"))
                .build();

        when(service.findPriceByProductBrandAppliedDate(findPriceQueryRequest)).thenThrow(ProductNotFoundException.class);

        // When
        mvc.perform(MockMvcRequestBuilders
                        .get("/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_dattime}",
                                1,1,"2020-06-16T21:00:00.000Z")
                        .contentType(MediaType.APPLICATION_JSON))

                // Then
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value("Not Found"));
        verify(service, times(1)).findPriceByProductBrandAppliedDate(findPriceQueryRequest);
    }
}