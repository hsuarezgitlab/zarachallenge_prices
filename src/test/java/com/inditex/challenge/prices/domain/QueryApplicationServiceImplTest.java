package com.inditex.challenge.prices.domain;

import com.inditex.challenge.prices.domain.core.entity.Price;
import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.price.*;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import com.inditex.challenge.prices.domain.mapper.ProductDataMapper;
import com.inditex.challenge.prices.domain.ports.service.QueryApplicationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class QueryApplicationServiceImplTest {

    @Autowired
    QueryApplicationService queryApplicationService;
    @MockBean
    private PriceQueryHelper priceQueryHelper;
    @MockBean
    private ProductDataMapper productDataMapper;

    @Test
    void findPriceByProductBrandAppliedDate() {
        // Given
        FindPriceQueryRequest findPriceQueryRequest = FindPriceQueryRequest.builder()
                .brandId(1)
                .productId(1)
                .appliedDateTime(ZonedDateTime.parse("2020-06-14T16:00:00.000Z"))
                .build();
        FindPriceQueryResponse findPriceQueryResponse = new FindPriceQueryResponse(
                1,
                1,
                2,
                ZonedDateTime.parse("2020-06-14T15:00:00.000Z"),
                ZonedDateTime.parse("2020-06-14T18:30:00.999Z"),
                BigDecimal.valueOf(25.45));

        ProductAggregate productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(
                        List.of(Price.Builder.newBuilder()
                                .brandId(new BrandId(1))
                                .startDate(new StartDate(ZonedDateTime.parse("2020-06-14T15:00:00.000Z")))
                                .endDate(new EndDate(ZonedDateTime.parse("2020-06-14T18:30:00.000Z")))
                                .priceList(new PriceList(2))
                                .priority(new Priority(1))
                                .price(BigDecimal.valueOf(20))
                                .currency(new Currency("EUR"))
                                .build()))
                .build();

        when(priceQueryHelper.findPriceByProductBrandAppliedDate(findPriceQueryRequest)).thenReturn(productAggregate);
        when(productDataMapper.productToFindPriceQueryResponse(
                productAggregate,
                findPriceQueryRequest.getAppliedDateTime(),
                new BrandId(findPriceQueryRequest.getBrandId()))).thenReturn(findPriceQueryResponse);

        // When
        FindPriceQueryResponse actualFindPriceQueryResponse = queryApplicationService.findPriceByProductBrandAppliedDate(findPriceQueryRequest);

        // Then
        Assertions.assertEquals(findPriceQueryResponse, actualFindPriceQueryResponse);
    }
}