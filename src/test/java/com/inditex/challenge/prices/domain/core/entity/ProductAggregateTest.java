package com.inditex.challenge.prices.domain.core.entity;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.price.*;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.core.exception.PriceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ProductAggregateTest {
    private List<Price> priceList;
    private ProductAggregate productAggregate;
    private BrandId brandId;
    private ZonedDateTime appliedDate;
    
    @BeforeEach
    public void setUp() {
        priceList = List.of(
                Price.Builder.newBuilder()
                        .brandId(new BrandId(1))
                        .startDate(new StartDate(ZonedDateTime.parse("2020-06-14T00:00:00.000Z")))
                        .endDate(new EndDate(ZonedDateTime.parse("2020-12-31T23:59:59.999Z")))
                        .priceList(new PriceList(4))
                        .priority(new Priority(0))
                        .price(BigDecimal.TEN)
                        .currency(new Currency("EUR"))
                        .build(),
                Price.Builder.newBuilder()
                        .brandId(new BrandId(1))
                        .startDate(new StartDate(ZonedDateTime.parse("2020-06-14T15:00:00.000Z")))
                        .endDate(new EndDate(ZonedDateTime.parse("2020-06-14T18:30:00.000Z")))
                        .priceList(new PriceList(2))
                        .priority(new Priority(1))
                        .price(BigDecimal.valueOf(20))
                        .currency(new Currency("EUR"))
                        .build()
        );
    }

    @Test
    void givenTwoPricesWithMatchDates_whenGetPrice_thenReturnPriceWithBiggestPriority() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(1);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(20);

        // When
        BigDecimal actualPrice = productAggregate.getPriceAtSpecificDate(appliedDate, brandId);

        // Then
        Assertions.assertEquals(expectedPrice, actualPrice);
    }

    @Test
    void givenTwoPricesWithMatchDates_whenGetBrand_thenReturnBrandWithBiggestPriority() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(1);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        int expectedBrandId = 1;

        // When
        int actualBrandId = productAggregate.getBrandIdAtSpecificDate(appliedDate, brandId);

        // Then
        Assertions.assertEquals(expectedBrandId, actualBrandId);
    }

    @Test
    void givenTwoPricesWithMatchDates_whenGetPriceList_thenReturnPriceListWithBiggestPriority() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(1);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        int expectedPriceList = 2;

        // When
        int actualPriceList = productAggregate.getPriceListAtSpecificDate(appliedDate, brandId);

        // Then
        Assertions.assertEquals(expectedPriceList, actualPriceList);
    }

    @Test
    void givenTwoPricesWithMatchDates_whenGetStartDate_thenReturnStartDateWithBiggestPriority() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(1);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-14T15:00:00.000Z");

        // When
        ZonedDateTime actualStartDate = productAggregate.getPriceStartDateAtSpecificDate(appliedDate, brandId);

        // Then
        Assertions.assertEquals(expectedStartDate, actualStartDate);
    }

    @Test
    void givenTwoPricesWithMatchDates_whenGetEndDate_thenReturnEndDateWithBiggestPriority() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(1);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-06-14T18:30:00.000Z");

        // When
        ZonedDateTime actualEndDate = productAggregate.getPriceEndDateAtSpecificDate(appliedDate, brandId);

        // Then
        Assertions.assertEquals(expectedEndDate, actualEndDate);

    }

    @Test
    void givenTwoPricesWithMatchDatesAndNotMatchWithGivenBrand_whenGetPrice_thenThrowsNewPriceNotFoundException() {
        // Given
        productAggregate = ProductAggregate.Builder.newBuilder()
                .id(new ProductId(1))
                .prices(priceList)
                .build();
        brandId = new BrandId(100);
        appliedDate = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-06-14T18:30:00.000Z");

        // When // Then
        Assertions.assertThrows(PriceNotFoundException.class, () -> productAggregate.getPriceEndDateAtSpecificDate(appliedDate, brandId));
    }
}