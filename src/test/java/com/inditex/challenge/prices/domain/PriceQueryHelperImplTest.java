package com.inditex.challenge.prices.domain;

import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandName;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.core.exception.BrandNotFoundException;
import com.inditex.challenge.prices.domain.core.exception.ProductNotFoundException;
import com.inditex.challenge.prices.domain.dto.FindPriceQueryRequest;
import com.inditex.challenge.prices.domain.ports.repository.BrandRepository;
import com.inditex.challenge.prices.domain.ports.repository.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@SpringBootTest
class PriceQueryHelperImplTest {
    @Autowired
    PriceQueryHelper priceQueryHelper;
    @MockBean
    ProductRepository productRepository;
    @MockBean
    BrandRepository brandRepository;

    @Test
    void givenFindPriceQueryRequestWithNonExistentProduct_whenFindProduct_thenThrowsProductNotFoundException() {
        // Given
        FindPriceQueryRequest findPriceQueryRequest = FindPriceQueryRequest.builder()
                .brandId(1)
                .productId(1)
                .appliedDateTime(ZonedDateTime.parse("2020-06-14T16:00:00.000Z"))
                .build();
        when(brandRepository.findById(new BrandId(1)))
                .thenReturn(Optional.of(Brand.Builder.newBuilder()
                        .id(new BrandId(1))
                        .name(new BrandName("ZARA"))
                        .build()));
        when(productRepository.findById(new ProductId(1))).thenThrow(ProductNotFoundException.class);

        // When Then
        Assertions.assertThrows(ProductNotFoundException.class, () -> priceQueryHelper.findPriceByProductBrandAppliedDate(findPriceQueryRequest));
    }

    @Test
    void givenFindPriceQueryRequestWithNonExistentBrand_whenFindProduct_thenThrowsProductNotFoundException() {
        // Given
        FindPriceQueryRequest findPriceQueryRequest = FindPriceQueryRequest.builder()
                .brandId(1000)
                .productId(1)
                .appliedDateTime(ZonedDateTime.parse("2020-06-14T16:00:00.000Z"))
                .build();
        when(brandRepository.findById(new BrandId(1000))).thenThrow(BrandNotFoundException.class);

        // When Then
        Assertions.assertThrows(BrandNotFoundException.class, () -> priceQueryHelper.findPriceByProductBrandAppliedDate(findPriceQueryRequest));
    }
}