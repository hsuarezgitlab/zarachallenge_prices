package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.mapper;

import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class BrandEntityMapperTest {
    @Test
    void givenABrandEntity_whenBrandEntityToBrand_thenReturnsBrand() {
        // Given
        BrandEntityMapper mapper = new BrandEntityMapper();
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setId(1);
        brandEntity.setName("ZARA");

        // When
        Brand brand = mapper.brandEntityToBrand(brandEntity);

        // Then
        assertEquals(brandEntity.getId(), brand.getId().getValue());
    }
}