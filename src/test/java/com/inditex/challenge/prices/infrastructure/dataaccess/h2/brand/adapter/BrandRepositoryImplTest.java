package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.adapter;

import com.inditex.challenge.prices.domain.core.entity.Brand;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandName;
import com.inditex.challenge.prices.domain.ports.repository.BrandRepository;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.mapper.BrandEntityMapper;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.repository.BrandJpaRepository;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class BrandRepositoryImplTest {
    @MockBean
    BrandEntityMapper mapper;

    @MockBean
    BrandJpaRepository brandJpaRepository;

    @Autowired
    BrandRepository brandRepository;

    @Test
    void findById() {
        // Given
        BrandId brandId = new BrandId(1);
        Brand brand = Brand.Builder.newBuilder()
                .id(brandId)
                .name(new BrandName("ZARA"))
                .build();
        BrandEntity brandEntity = new BrandEntity();
        brandEntity.setId(brandId.getValue());
        brandEntity.setName("Some name");
        brandEntity.setPrices(List.of(new PriceEntity()));

        when(brandJpaRepository.findById(brandId.getValue())).thenReturn(Optional.of(brandEntity));
        when(mapper.brandEntityToBrand(brandEntity)).thenReturn(brand);

        // When
        Optional<Brand> actualBrand = brandRepository.findById(brandId);

        // Then
        Assertions.assertTrue(actualBrand.isPresent());
        Assertions.assertEquals(1, actualBrand.get().getId().getValue());
    }
}