package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.repository;

import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

@DataJpaTest
class ProductJpaRepositoryTest {

    @Autowired
    ProductJpaRepository repository;

    @Test
    void givenProductIdExistent_whenFindById_thenReturnOptionalOfProductEntity(){
        // Given
        ProductId productId = new ProductId(1);

        // When
        Optional<ProductEntity> productEntity = repository.findById(productId.getValue());

        // Then
        Assertions.assertTrue(productEntity.isPresent());
        Assertions.assertEquals(1, productEntity.get().getId());
    }

    @Test
    void givenProductIdNonExistent_whenFindById_thenReturnEmptyOptional(){
        // Given
        ProductId productId = new ProductId(10000);

        // When
        Optional<ProductEntity> productEntity = repository.findById(productId.getValue());

        // Then
        Assertions.assertTrue(productEntity.isEmpty());
    }
}