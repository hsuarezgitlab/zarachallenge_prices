package com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.repository;

import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class BrandJpaRepositoryTest {
    @Autowired
    BrandJpaRepository repository;

    @Test
    void givenBrandIdExistent_whenFindById_thenReturnOptionalOfBrandEntity(){
        // Given
        BrandId brandId = new BrandId(1);

        // When
        Optional<BrandEntity> brandEntity = repository.findById(brandId.getValue());

        // Then
        Assertions.assertTrue(brandEntity.isPresent());
        Assertions.assertEquals(1, brandEntity.get().getId());
        Assertions.assertEquals("ZARA", brandEntity.get().getName());
    }

    @Test
    void givenBrandIdNonExistent_whenFindById_thenReturnEmptyOptional(){
        // Given
        BrandId brandId = new BrandId(1000);

        // When
        Optional<BrandEntity> brandEntity = repository.findById(brandId.getValue());

        // Then
        Assertions.assertTrue(brandEntity.isEmpty());
    }
}