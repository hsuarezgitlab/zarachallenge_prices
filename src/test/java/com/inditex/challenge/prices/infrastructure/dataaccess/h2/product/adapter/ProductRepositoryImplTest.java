package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.adapter;

import com.inditex.challenge.prices.domain.core.entity.Price;
import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.domain.core.entity.valueobject.brand.BrandId;
import com.inditex.challenge.prices.domain.core.entity.valueobject.price.*;
import com.inditex.challenge.prices.domain.core.entity.valueobject.product.ProductId;
import com.inditex.challenge.prices.domain.ports.repository.ProductRepository;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.mapper.ProductEntityMapper;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.repository.ProductJpaRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;

@SpringBootTest
class ProductRepositoryImplTest {

    @MockBean
    ProductEntityMapper mapper;

    @MockBean
    ProductJpaRepository productJpaRepository;

    @Autowired
    ProductRepository productRepository;

    @Test
    void findByIdTest() {
        // Given
        ProductId productId = new ProductId(1);
        ProductAggregate productAggregate = ProductAggregate.Builder.newBuilder()
                        .id(productId)
                                .prices(List.of(Price.Builder.newBuilder()
                                        .brandId(new BrandId(1))
                                        .startDate(new StartDate(ZonedDateTime.parse("2020-06-14T15:00:00.000Z")))
                                        .endDate(new EndDate(ZonedDateTime.parse("2020-06-14T18:30:00.000Z")))
                                        .priceList(new PriceList(2))
                                        .priority(new Priority(1))
                                        .price(BigDecimal.valueOf(20))
                                        .currency(new Currency("EUR"))
                                        .build())).build();
        ProductEntity productEntity = new ProductEntity();
        productEntity.setId(productId.getValue());
        productEntity.setName("Some name");
        productEntity.setPrices(List.of(new PriceEntity()));

        when(productJpaRepository.findById(productId.getValue())).thenReturn(Optional.of(productEntity));
        when(mapper.productEntityToProductAggregate(productEntity)).thenReturn(productAggregate);


        // When
       Optional<ProductAggregate> actualProduct = productRepository.findById(productId);

        // Then
        Assertions.assertTrue(actualProduct.isPresent());
        Assertions.assertEquals(1, actualProduct.get().getId().getValue());
    }
}