package com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.mapper;

import com.inditex.challenge.prices.domain.core.entity.Price;
import com.inditex.challenge.prices.domain.core.entity.ProductAggregate;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.brand.entity.BrandEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.price.entity.PriceEntity;
import com.inditex.challenge.prices.infrastructure.dataaccess.h2.product.entity.ProductEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.List;

class ProductEntityMapperTest {
    ProductEntityMapper mapper;
    ProductEntity productEntity;
    BrandEntity brandEntity;
    PriceEntity priceEntity;

    @BeforeEach
    void setUp(){
        mapper = new ProductEntityMapper();
        productEntity = new ProductEntity();
        productEntity.setId(1);
        productEntity.setName("product 1");

        brandEntity = new BrandEntity();
        brandEntity.setId(1);
        brandEntity.setName("ZARA");

        priceEntity = new PriceEntity();
        priceEntity.setId(1);
        priceEntity.setBrand(brandEntity);
        priceEntity.setProduct(productEntity);
        priceEntity.setPrice(BigDecimal.TEN);
        priceEntity.setPriceList(4);
        priceEntity.setPriority(1);
        priceEntity.setCurr("EUR");
        priceEntity.setStartDate(ZonedDateTime.parse("2020-06-14T15:00:00.000Z"));
        priceEntity.setEndDate(ZonedDateTime.parse("2020-06-16T15:00:00.000Z"));
        productEntity.setPrices(List.of(priceEntity));
    }

    @Test
    void givenAProductEntity_whenProductEntityToProductAggregate_thenReturnsProductAggregate() {
        // Given
        ProductEntity productEntityTest1 = productEntity;

        // When
        ProductAggregate product = mapper.productEntityToProductAggregate(productEntityTest1);

        // Then
        Assertions.assertNotNull(product);
        Assertions.assertEquals(product.getId().getValue(), productEntityTest1.getId());
    }

    @Test
    void givenAListAPriceEntities_whenPriceEntitiesToPrices_thenReturnsListOfPrices() {
        // Given
        List<PriceEntity> priceEntityListTest2 = List.of(priceEntity);

        // When
        List<Price> prices = mapper.priceEntitiesToPrices(priceEntityListTest2);

        // Then
        Assertions.assertNotNull(prices);
        Assertions.assertEquals(1, prices.size());
        Assertions.assertEquals(priceEntityListTest2.get(0).getPrice(), prices.get(0).getPrice());
        Assertions.assertEquals(priceEntityListTest2.get(0).getPriceList(), prices.get(0).getPriceList().getValue());
        Assertions.assertEquals(priceEntityListTest2.get(0).getPriority(), prices.get(0).getPriority().getValue());
        Assertions.assertEquals(priceEntityListTest2.get(0).getCurr(), prices.get(0).getCurrency().getValue());
        Assertions.assertEquals(priceEntityListTest2.get(0).getStartDate(), prices.get(0).getStartDate().getValue());
        Assertions.assertEquals(priceEntityListTest2.get(0).getEndDate(), prices.get(0).getEndDate().getValue());
    }

    @Test
    void givenAPriceEntity_whenPriceEntityToPrice_thenReturnsPrice() {
        // Given
        PriceEntity priceEntityTest3 = priceEntity;

        // When
        Price price = mapper.priceEntityToPrice(priceEntityTest3);

        // Given
        Assertions.assertNotNull(price);
        Assertions.assertEquals(priceEntityTest3.getPrice(), price.getPrice());
        Assertions.assertEquals(priceEntityTest3.getPriceList(), price.getPriceList().getValue());
        Assertions.assertEquals(priceEntityTest3.getPriority(), price.getPriority().getValue());
        Assertions.assertEquals(priceEntityTest3.getCurr(), price.getCurrency().getValue());
        Assertions.assertEquals(priceEntityTest3.getStartDate(), price.getStartDate().getValue());
        Assertions.assertEquals(priceEntityTest3.getEndDate(), price.getEndDate().getValue());
    }
}