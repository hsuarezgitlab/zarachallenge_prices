package com.inditex.challenge.prices.integration;

import com.inditex.challenge.prices.domain.dto.FindPriceQueryResponse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class HttpRequestTest {
    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    void required_test_1() throws Exception {
        // Given
        int productId = 35455;
        int brandId = 1;
        ZonedDateTime appliedDateTime = ZonedDateTime.parse("2020-06-14T10:00:00.000Z");

        int expectedProductId = 35455;
        int expectedBrandId = 1;
        int expectedPriceList = 1;
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-14T00:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-12-31T23:59:59.999Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(35.50).setScale(2, RoundingMode.CEILING);

        ResponseEntity<FindPriceQueryResponse> response =
                restTemplate
                        .getForEntity("http://localhost:" + port + "/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_datetime}",
                                FindPriceQueryResponse.class,
                                productId,
                                brandId,
                                appliedDateTime);
        FindPriceQueryResponse resultTest = response.getBody();

        Assertions.assertNotNull(resultTest);
        Assertions.assertEquals(expectedProductId, resultTest.productId());
        Assertions.assertEquals(expectedBrandId, resultTest.brandId());
        Assertions.assertEquals(expectedPriceList, resultTest.priceList());
        Assertions.assertEquals(expectedStartDate, resultTest.startDate());
        Assertions.assertEquals(expectedEndDate, resultTest.endDate());
        Assertions.assertEquals(expectedPrice, resultTest.price());
    }

    @Test
    void required_test_2() throws Exception {
        // Given
        int productId = 35455;
        int brandId = 1;
        ZonedDateTime appliedDateTime = ZonedDateTime.parse("2020-06-14T16:00:00.000Z");

        int expectedProductId = 35455;
        int expectedBrandId = 1;
        int expectedPriceList = 2;
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-14T15:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-06-14T18:30:00.000Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(25.45);

        ResponseEntity<FindPriceQueryResponse> response =
                restTemplate
                        .getForEntity("http://localhost:" + port + "/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_datetime}",
                                FindPriceQueryResponse.class,
                                productId,
                                brandId,
                                appliedDateTime);
        FindPriceQueryResponse resultTest = response.getBody();

        Assertions.assertNotNull(resultTest);
        Assertions.assertEquals(expectedProductId, resultTest.productId());
        Assertions.assertEquals(expectedBrandId, resultTest.brandId());
        Assertions.assertEquals(expectedPriceList, resultTest.priceList());
        Assertions.assertEquals(expectedStartDate, resultTest.startDate());
        Assertions.assertEquals(expectedEndDate, resultTest.endDate());
        Assertions.assertEquals(expectedPrice, resultTest.price());
    }

    @Test
    void required_test_3() throws Exception {
        // Given
        int productId = 35455;
        int brandId = 1;
        ZonedDateTime appliedDateTime = ZonedDateTime.parse("2020-06-14T21:00:00.000Z");

        int expectedProductId = 35455;
        int expectedBrandId = 1;
        int expectedPriceList = 1;
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-14T00:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-12-31T23:59:59.999Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(35.50).setScale(2, RoundingMode.CEILING);

        ResponseEntity<FindPriceQueryResponse> response =
                restTemplate
                        .getForEntity("http://localhost:" + port + "/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_datetime}",
                                FindPriceQueryResponse.class,
                                productId,
                                brandId,
                                appliedDateTime);
        FindPriceQueryResponse resultTest = response.getBody();

        Assertions.assertNotNull(resultTest);
        Assertions.assertEquals(expectedProductId, resultTest.productId());
        Assertions.assertEquals(expectedBrandId, resultTest.brandId());
        Assertions.assertEquals(expectedPriceList, resultTest.priceList());
        Assertions.assertEquals(expectedStartDate, resultTest.startDate());
        Assertions.assertEquals(expectedEndDate, resultTest.endDate());
        Assertions.assertEquals(expectedPrice, resultTest.price());
    }

    @Test
    void required_test_4() throws Exception {
        // Given
        int productId = 35455;
        int brandId = 1;
        ZonedDateTime appliedDateTime = ZonedDateTime.parse("2020-06-15T10:00:00.000Z");

        int expectedProductId = 35455;
        int expectedBrandId = 1;
        int expectedPriceList = 3;
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-15T00:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-06-15T11:00:00.000Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(30.50).setScale(2, RoundingMode.CEILING);

        ResponseEntity<FindPriceQueryResponse> response =
                restTemplate
                        .getForEntity("http://localhost:" + port + "/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_datetime}",
                                FindPriceQueryResponse.class,
                                productId,
                                brandId,
                                appliedDateTime);
        FindPriceQueryResponse resultTest = response.getBody();

        Assertions.assertNotNull(resultTest);
        Assertions.assertEquals(expectedProductId, resultTest.productId());
        Assertions.assertEquals(expectedBrandId, resultTest.brandId());
        Assertions.assertEquals(expectedPriceList, resultTest.priceList());
        Assertions.assertEquals(expectedStartDate, resultTest.startDate());
        Assertions.assertEquals(expectedEndDate, resultTest.endDate());
        Assertions.assertEquals(expectedPrice, resultTest.price());
    }

    @Test
    void required_test_5() throws Exception {
        // Given
        int productId = 35455;
        int brandId = 1;
        ZonedDateTime appliedDateTime = ZonedDateTime.parse("2020-06-16T21:00:00.000Z");

        int expectedProductId = 35455;
        int expectedBrandId = 1;
        int expectedPriceList = 4;
        ZonedDateTime expectedStartDate = ZonedDateTime.parse("2020-06-15T16:00:00.000Z");
        ZonedDateTime expectedEndDate = ZonedDateTime.parse("2020-12-31T23:59:59.999Z");
        BigDecimal expectedPrice = BigDecimal.valueOf(38.95).setScale(2, RoundingMode.CEILING);

        ResponseEntity<FindPriceQueryResponse> response =
                restTemplate
                        .getForEntity("http://localhost:" + port + "/products/{product_id}/price?brand_id={brand_id}&applied_datetime={applied_datetime}",
                                FindPriceQueryResponse.class,
                                productId,
                                brandId,
                                appliedDateTime);
        FindPriceQueryResponse resultTest = response.getBody();

        Assertions.assertNotNull(resultTest);
        Assertions.assertEquals(expectedProductId, resultTest.productId());
        Assertions.assertEquals(expectedBrandId, resultTest.brandId());
        Assertions.assertEquals(expectedPriceList, resultTest.priceList());
        Assertions.assertEquals(expectedStartDate, resultTest.startDate());
        Assertions.assertEquals(expectedEndDate, resultTest.endDate());
        Assertions.assertEquals(expectedPrice, resultTest.price());
    }
}
